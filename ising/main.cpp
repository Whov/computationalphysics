#include <fstream>
#include <iostream>
#include <vector>
#include <cstring>

#include "dtoa_milo.h"
#include "../Ran2.cpp"

using namespace std;

void save_on_file(const string filename, const vector<double> &A);

const int estrazioni = 1000000;

int main()
{
	long id = -921436587;
	vector<double> rand;
	rand.resize(estrazioni);
	for (int i=0;i<estrazioni;i++) {
		rand[i] = ran2(&id);
	}
	double corr = 0.;
	for (int i=0;i<estrazioni-1;i++) {
		corr += (rand[i]-0.5)*(rand[i+1]-0.5);
	}
	cout << "Correlazione fra primi vicini: " << corr/estrazioni << endl;
	//save_on_file("rand.txt", rand);
    //cout << "Stampato rand.txt. Plottalo!" << endl;
}

// write on file
inline void fast_write_string(FILE *out, char *str) {
    int i = 0;
    while (str[i] != '\0') {
	putc_unlocked(str[i], out);
	++i;
    }
}

// save to file txt
void save_on_file(const string filename,
                  const vector<double> &A) {
    // Open the file
    FILE *out = fopen(filename.c_str(), "w");
    char temp_string[25];
    int i;
    /* rand(j), j\in[0,99] */
    for (unsigned int i = 0; i < estrazioni/2; ++i) {
	    dtoa_milo(A[2*i], temp_string);// copia in modo veloce sul buffer
	    fast_write_string(out, temp_string);// scrivi in modo veloce sul file
	    putc_unlocked(' ', out);
	    dtoa_milo(A[2*i+1], temp_string);
	    fast_write_string(out, temp_string);
	    putc_unlocked('\n', out);
    }
    fclose(out);
}
