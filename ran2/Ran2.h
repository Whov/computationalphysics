namespace ran2{
	void set_seed(unsigned int seed);
	double ran2();
	double gauss_ran2(double mean, double stddev);
}
