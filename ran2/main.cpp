#include <iostream>
#include <chrono> 

#include "Ran2.h"

#define N 1000000
#define SEED 42

using namespace std::chrono; //mi serve per misurare la velocit�

int main(int argc, char** argv) {
	
	//uniform
	// prendo il tempo di inizio
    auto ran2_start = high_resolution_clock::now(); 
    
	ran2::set_seed(SEED); //setto il seme
	for(int i=0; i<N; i++){
		ran2::ran2(); //estraggo un numero casuale
	}
	//calcolo il tempo passato
    auto ran2_time = duration_cast<microseconds>(high_resolution_clock::now() - ran2_start); 
    
    //gaussian
	// prendo il tempo di inizio
    auto ran2_gauss_start = high_resolution_clock::now(); 
    
	ran2::set_seed(SEED); //setto il seme
	for(int i=0; i<N; i++){
		ran2::gauss_ran2(0, 1); //estraggo un numero casuale con media 0 e deviazione 1
	}
	//calcolo il tempo passato
    auto ran2_gauss_time = duration_cast<microseconds>(high_resolution_clock::now() - ran2_gauss_start); 
	
	std::cout << "Extracting " << N << " uniform  distribution: " << ran2_time.count() << " microseconds" << std::endl;
	std::cout << "Extracting " << N << " gaussian distribution: " << ran2_gauss_time.count() << " microseconds" << std::endl;
	
	return 0;
}
